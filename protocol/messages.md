# Messages

Les messages sont envoyés en utilisant la librairie `socketio`, qui utilise principalement la technologie websocket.

## Événements envoyés par l'utilisateur

Ces événements sont traités par le serveur.

### Join (`join`)

Rejoindre la session collaborative

**Données**

- `username`, *optionnel* : rejoindre en utilisant un nom d'utilisateur spécifique


### Update (`update`)

Le texte a été modifié localement dans un IDE : les modifications doivent être envoyées au serveur.

**Données**
- `cursor`, *optionnel* : position du curseur
- `operation`, *optionnel* : opération

**Exemples**
```json
{
 "cursor": {"line": 12, "column": 35}
}
```

```json
{
 "operation": {"type": "insert_char", "position": {"line": 12, "column": 0}, "text": "b"}
}
```

```json
{
 "operation": {"type": "delete_char", "position": {"line": 12, "column": 0}, "text": "b"}
}
```

## Events sent by the server

Ces événements sont reçus et traités par les plugins

### Update (`update`)

Le texte a été mis à jour sur le serveur par un autre IDE, et cela doit être pris en compte par l'IDE localement

**Data**
- `uuid` : uuid de celui qui a modifié
- `username` *optionnel* : nom d'utilisateur de celui qui a modifié
- `operation` : opération effectuée par l'utilisateur

**Examples**
 ```json
 {
     "uuid": "397"
     "operation": {"type": "instert_char", "position": {"line": 12, "column": 0}, "text": "b"}
 }
 ```

 ```json
 {
     "uuid": "397"
     "cursor": {"line": 12, "column": 35}
 }
 ```
