# Pages

## Protocole

- [messages](./protocol/messages.md)

## Infrastructure

- [défini l'infrastructure en place](./infrastucture/server.md)

# Repo spécifiant les spécifications techniques du projet

Ce repo contiendra notament l'architecture, les structures de données ainsi que la formalisation des différents échanges entre les entités (serveur, plugin Atom, plugin VSCode)
